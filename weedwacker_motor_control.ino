#define ON 1
#define OFF 0

int activateMotor = 0; // need information from controls group

void setup() {
  // Set pin 6 as Digital Output
  pinMode(6, OUTPUT);
}

void loop() {
  
  //need another if/else statement to let activateMotor turn on or off based on signal from control group
  
  if (activateMotor == ON) {
    digitalWrite(6, LOW); //turn MOTOR ON
    delay(1000);
  }
  else {
    digitalWrite(6, HIGH); // turn MOTOR OFF
    delay(1000)
  }
  Serial.print("Motor Status: ") ;  // Prints the motor status on Serial Monitor
  Serial.println(activateMotor);
  
}
